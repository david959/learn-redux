import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Provider } from "react-redux";

import { store } from "redux/store";
import Counter from "components/Counter/Counter";
import Todo from "components/Todo/Todo";
import Home from "components/Home/Home";

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/counter" element={<Counter />} />
          <Route path="/todo" element={<Todo />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
