import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "redux/reducer/counterSlice";
import todoReducer from "redux/reducer/todoSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    todo: todoReducer,
  },
});
