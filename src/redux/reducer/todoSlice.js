import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: [],
};

export const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    addTodo: (state, data) => {
      state.value.unshift(data.payload);
    },
    editTodo: (state, data) => {
      state.value[data.payload.index] = data.payload.updateData;
    },
    removeTodo: (state, data) => {
      state.value.splice(data.payload, 1);
    },
  },
});

export const { addTodo, editTodo, removeTodo } = todoSlice.actions;

export default todoSlice.reducer;
