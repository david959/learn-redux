import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { decrement, increment } from "redux/reducer/counterSlice";

const Counter = () => {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();
  return (
    <>
      <div>
        <p>Increment</p>
        <button onClick={() => dispatch(increment())}>Plus</button>
      </div>
      <div>
        <p>Decrement</p>
        <button onClick={() => dispatch(decrement())}>Sub</button>
      </div>
      <div>
        <p>{count}</p>
      </div>
    </>
  );
};

export default Counter;
