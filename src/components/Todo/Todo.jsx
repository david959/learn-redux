import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Select from "react-select";

import { addTodo, editTodo, removeTodo } from "redux/reducer/todoSlice";
import { options } from "utils/dropOption";
import { stateValue } from "utils/useStateDefault";

import "./todo.css";

const Todo = () => {
  let todo = useSelector((state) => state.todo.value);
  const dispatch = useDispatch();

  const [input, setInput] = useState(stateValue);
  const [editAdd, setEditAdd] = useState(false);
  const [editIndex, setEditIndex] = useState();

  const inputHandler = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const dropHandler = (e) => {
    setInput({ ...input, status: e.value });
  };

  const addHandler = () => {
    dispatch(addTodo(input));
    setInput(stateValue);
  };

  const editHandler = (index) => {
    setInput({
      todo: todo[index].todo,
      name: todo[index].name,
      status: todo[index].status,
    });
    setEditAdd(true);
    setEditIndex(index);
  };

  const addEditHandler = (toEditIndex) => {
    const toEditData = {
      index: toEditIndex,
      updateData: input,
    };
    dispatch(editTodo(toEditData));
    setInput(stateValue);
    setEditAdd(false);
  };

  return (
    <>
      <div className="input">
        <span>Enter todo</span>
        <input
          type="text"
          name="todo"
          value={input.todo}
          placeholder="Enter you input"
          onChange={inputHandler}
        />
        <span>Assignee Name</span>
        <input
          type="text"
          name="name"
          value={input.name}
          placeholder="Enter assignee name"
          onChange={inputHandler}
        />
        <span>Status</span>
        <Select
          value={options.filter(({ value }) => value === input.status)}
          onChange={dropHandler}
          options={options}
        />
      </div>
      <div className="todo-list">
        <div className="todo-wrap">
          {todo.map((data, index) => (
            <div className="todo-card" key={index}>
              <div className="todo-todos">
                <p>
                  <span>Todo:</span> {data.todo}
                </p>
              </div>
              <div className="todo-assignee">
                <p>
                  <span>Assignee:</span> {data.name}
                </p>
              </div>
              <div className="todo-status">
                <p>
                  <span>Status:</span> {data.status}
                </p>
              </div>
              <div className="todo-status">
                <p>
                  <button onClick={() => editHandler(index)}>Edit</button>{" "}
                  <button onClick={() => dispatch(removeTodo(index))}>
                    Remove
                  </button>
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div>
        {editAdd ? (
          <button onClick={() => addEditHandler(editIndex)}>UPDATE</button>
        ) : (
          <button onClick={addHandler}>ADD</button>
        )}
      </div>
    </>
  );
};

export default Todo;
