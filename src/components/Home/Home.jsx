import React from "react";
import { NavLink } from "react-router-dom";

const Home = () => {
  return (
    <>
      <NavLink
        to="/counter"
        className={(isActive) => "nav-link" + (!isActive ? " unselected" : "")}
      >
        Counter
      </NavLink>
      <NavLink
        to="/todo"
        className={(isActive) => "nav-link" + (!isActive ? " unselected" : "")}
      >
        Todo
      </NavLink>
    </>
  );
};

export default Home;
